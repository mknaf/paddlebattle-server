#!/usr/bin/env python
# -*- coding: utf-8 -*-

import flask_socketio
import logging
import logging.handlers

from flask import Flask, request
from flask_socketio import SocketIO, emit, join_room

from PaddleBattle.Game import Game

app = Flask(__name__)
app.config["SECRET_KEY"] = "uirewkjnds79834kjerw76vcxjwer"
socketio = SocketIO(app)

# get a new logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# how to format log messages
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
# a handler to log to console
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logging.basicConfig(level=logging.DEBUG)


games = {}
players = {}


def get_game():
    logger.info("Trying to find game with free slot.")

    # for all the games in our list
    for ident, game in games.iteritems():

        # if there is at least 1 free slot in this game
        if game.has_space:

            # return game's ident
            return ident

    # if there were no games, create a new GameWorker
    logger.info("No games with an empty slot found, Creating new one.")
    g = Game()
    g.start()

    # add it to the list of games
    games[g.ident] = g

    # return new game's ident
    return g.ident

def get_gamestate(game_id):
    return games[game_id].get_gamestate()


@socketio.on("get_gamestate")
def on_get_gamestate():
    player_id = request.sid
    emit(
        "get_gamestate",
        get_gamestate(players[player_id])
    )


@socketio.on("join")
def on_join():
    player_id = request.sid
    logger.info("Player {} wants to join a game.".format(player_id))
    game_id = get_game()

    # add player to our game's players list
    logger.debug("Adding new player to game.")
    games[game_id].join(request.sid)

    # tell socketio that this player is in this room
    join_room(game_id)

    # remember this player and which room they're in
    players[request.sid] = game_id

    player_subid = games[game_id].player_subid(request.sid)

    # tell the new player about the current gamestate
    logger.debug("Sending gamestate to new player.")
    emit(
        "init_game",
        get_gamestate(game_id),
    )

    # tell everyone a new player has joined
    # WHILE making sure that that the new player doesn't receive
    # that 'player_join' event as well
    emit(
        "player_join",
        room=game_id,
        include_self=False
    )


@socketio.on("disconnect")
def on_disconnect():
    player_id = request.sid
    game_id = players[player_id]

    player_subid = games[game_id].player_subid(player_id)

    # broadcast to the game that this player has left
    emit(
        "player_leave",
        player_subid,
        room=game_id
    )

    # remove player from the game
    games[game_id].disconnect(player_id)

    # remove player from our players list
    del players[player_id]

    # if the game now has no players left, delete it
    if games[game_id].is_empty:
        logger.debug("Game {} is now empty, closing it.".format(game_id))
        games[game_id].game_over
        del games[game_id]


@socketio.on("move_paddle")
def on_move_paddle(mov):
    """Tell all the players in a game who moved where."""
    room = players[request.sid]
    player_subid = games[room].player_subid(request.sid)
    player_id = request.sid

    # inform the server
    games[room].move_paddle(player_id, mov)

    # inform the other players
    emit(
        "move_paddle",
        (
            player_subid,
            mov
        ),
        room=room
    )


if __name__ == "__main__":
    socketio.run(app, debug=True)
