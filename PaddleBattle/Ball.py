#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random

class Ball(object):

    def __init__(self):
        self.x = 0.5
        self.y = 0.5

        self.velocity_x = 200

        self._angle = random.randint(0, 360)

    def get_coords(self):
        return {
            "x": self.x,
            "y": self.y
        }
