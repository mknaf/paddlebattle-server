#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging
import logging.handlers
import settings
import threading
import time

from uuid import uuid4

from Player import Player
from Ball import Ball

class Game(threading.Thread):

    def __init__(self):
        super(Game, self).__init__()

        self.logger = self._get_logger()

        self.stoprequest = threading.Event()

        self._ident = str(uuid4())

        self._players = {}
        self._max_players = settings.max_players

        self._balls = [
            Ball() for i in range(self._max_players)
        ]

    def run(self):
        """Where the action is."""
        while not self.stoprequest.isSet():

            starttime = time.time()

            # update gamestate

            for player_id, player in self._players.iteritems():
                if player.mov == "left":
                    player.pos = player.pos - 0.02
                if player.mov == "right":
                    player.pos = player.pos + 0.02
                if player.mov == "stop":
                    pass

            # sleep until it's time for an update
            duration = time.time() - starttime
            time.sleep((settings.game_update_rate - duration) / 1000)

    def get_gamestate(self):
        """Return a gamestate object."""
        players = sorted(
            [(player.subid, player.pos, player.mov) for player in self.players.values()],
            key=lambda x: x[0]
        )
        players = [
            {
                "pos": pos,
                "mov": mov,
                "subid": subid
            } for subid, pos, mov in players
        ]

        balls = [ball.get_coords() for ball in self._balls]

        return {
            "paddles": players,
            "balls": balls
        }


    @property
    def game_over(self, timeout=None):
        """Kill this thing gracefully from the outside."""
        self.stoprequest.set()
        super(Game, self).join(timeout)

    @property
    def players(self):
        return self._players

    @property
    def ident(self):
        return self._ident

    @property
    def has_space(self):
        if len(self._players) < self._max_players:
            return True
        return False

    @property
    def is_empty(self):
        if len(self._players) == 0:
            return True
        return False

    @property
    def is_full(self):
        return not self.has_space

    @property
    def player_count(self):
        return len(self._players)

    def move_paddle(self, player_id, mov):
        self._players[player_id].mov = mov

    def has_player(self, player):
        return player in self._players

    def player_subid(self, player):
        return self._players[player].subid

    def join(self, player):
        """Register a player in the list of players for this Game."""
        # if this player is already registered
        if player in self._players:
            raise Exception("Player is already in this game.")

        # if there is no more room for this player
        if len(self._players) >= self._max_players:
            raise Exception("Cannot join player, max_players reached.")

        self._players[player] = Player(subid=len(self._players))
        self.logger.info(
            "Player {} joined game {}. Now {} players.".format(
                player,
                self.ident,
                len(self._players)
            )
        )
        return True

    def disconnect(self, player):
        subid = self._players[player].subid
        del self._players[player]
        self.logger.info(
            "Player {} left game {}".format(
                player,
                self.ident
            )
        )

        # update all player subids
        for pid, p in self._players.iteritems():
            if subid < p.subid:
                p.subid = p.subid - 1

        return True

    def _get_logger(self):
        # get a new logger
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)

        # how to format log messages
        formatter = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )

        # a handler to log to console
        ch = logging.StreamHandler()
        ch.setFormatter(formatter)
        # only add the handler if we don't have a handler
        # of this type already. produces duplicate output otherwise
        if type(ch) not in [type(h) for h in logger.handlers]:
            logger.addHandler(ch)

        # a handler to log to rotating files
        """
        fh = logging.handlers.RotatingFileHandler(
            filename=settings.logfile,
            maxBytes=50000,
            backupCount=5,
        )
        fh.setFormatter(formatter)
        # only add the handler if we don't have a handler
        # of this type already. produces duplicate output otherwise
        if type(fh) not in [type(h) for h in logger.handlers]:
            logger.addHandler(fh)
        """

        return logger
