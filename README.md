# About
A Server for a multiplayer pong-like game called *PaddleBattle*.
The Server is written in Python and uses a [socket.io](http://socket.io)
implementation from [here](https://github.com/miguelgrinberg/Flask-SocketIO),
which is nicely documented [here](http://flask-socketio.readthedocs.org/en/latest/).


